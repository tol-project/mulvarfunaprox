/* -*- mode: C++ -*- */
//////////////////////////////////////////////////////////////////////////////
// FILE   : DesignC2.Monomial.Spheric.Delaunay2D.tol
// PURPOSE: Defines Class @DesignC2.Monomial.Spheric.Delaunay2D
//////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////
Text _.autodoc.member.DesignC2.Monomial.Spheric.Delaunay2D.tol = 
"";
Class @DesignC2.Monomial.Spheric.Delaunay2D : 
  @DesignC2.Monomial, 
  @DesignC2.Spheric
//////////////////////////////////////////////////////////////////////////////
{
//////////////////////////////////////////////////////////////////////////////
//Configuration parameters
//////////////////////////////////////////////////////////////////////////////
  Real stress.maxDistance;
  MatQuery::@TPP.Delaunay _.delaunay;
  Set _.circumcircles = Copy(Empty);
  Set _.faces  = Copy(Empty);
  ////////////////////////////////////////////////////////////////////////////
  Static @DesignC2.Monomial.Spheric.Delaunay2D New(
    VMatrix X, 
    Real maxNeighbour,
    Real radius.margin,
    Real stress.maxDistance_)
  ////////////////////////////////////////////////////////////////////////////
  {
    @DesignC2.Monomial.Spheric.Delaunay2D new =
    [[ 
      VMatrix _.X = X;
      Real _.n = VColumns(X);
      Real _.J = VRows(X);
      Real _.X.maxNeighbour = maxNeighbour+1;
      Real _.radius.margin = radius.margin;
      Real stress.maxDistance = stress.maxDistance_;
      MatQuery::@TPP.Delaunay _.delaunay = 
       MatQuery::@TPP.Delaunay::New(VMat2Mat(X))
    ]] 
  };

  //////////////////////////////////////////////////////////////////////////////
  Real build.reference.points(Real void)
  //////////////////////////////////////////////////////////////////////////////
  {
    Text _MID = "[@DesignC2.Monomial.Spheric.Delaunay2D::build.reference.points] ";
    Set _.faces := _.delaunay::get.faces(0);
    Set _.circumcircles := _.delaunay::get.circumcircles(0);
  //Matrix center = _.circumcircles::center;
    Matrix center = (_.faces::A+_.faces::B+_.faces::C)*(1/3);
    Real set.reference.points(Mat2VMat(center));
    VMatrix _.Yj := Constant(_.H,1,3);
    _.H
  };


  //////////////////////////////////////////////////////////////////////////////
  Real build.border.points(Real void)
  //////////////////////////////////////////////////////////////////////////////
  {
    VMatrix _.Yb := Zeros(_.H,1);
    Real _.Tb := 0
  };

  //////////////////////////////////////////////////////////////////////////////
  Real build.stress.points(Real void)
  //////////////////////////////////////////////////////////////////////////////
  {
    Text _MID = "[@DesignC2.Monomial.Spheric.Delaunay2D::build.stress.points] ";
    Real _.Z.maxNeighbour := If(prior.Z.order>=0,2,0);
    If(!_.Z.maxNeighbour, 
    {
      VMatrix _.Yt := Zeros(_.H,1);
      Real _.Tz := 0
    },{
    Set Y1_ = For(2,_.X.maxNeighbour,VMatrix(Real k)
    {
      Mat2VMat(SubCol(_.ngbh::_.neighbour,[[1]]))
    });
    Set Y2_ = For(2,_.X.maxNeighbour,VMatrix(Real k)
    {
      Mat2VMat(SubCol(_.ngbh::_.neighbour,[[k]]))
    });
    Set distance_ = For(2,_.X.maxNeighbour,VMatrix(Real k)
    {
      Mat2VMat(Sqrt(SubCol(_.ngbh::_.distance2,[[k]])))
    });
    Set match_ = For(1,_.X.maxNeighbour-1,VMatrix(Real k)
    {
      And(
        LE(distance_[k], Constant(_.H,1,stress.maxDistance)),
        LT(Y1_[k],Y2_[k]))
    });
    VMatrix distance.all = Group("ConcatRows",distance_);
    VMatrix Y1.all = Group("ConcatRows",Y1_);
    VMatrix Y2.all = Group("ConcatRows",Y2_);
    VMatrix match = Group("ConcatRows",match_);
    Set sel = MatQuery::SelectMatch(match);
    Real _.Tz := Card(sel);
    VMatrix distance = SubRow(distance.all,sel);
    VMatrix Y1 = SubRow(Y1.all,sel);
    VMatrix Y2 = SubRow(Y2.all,sel);
    VMatrix _.Z.h := Y1 | Y2;
    VMatrix y1 = SubRow(_.Y,VCol2Set(Y1));
    VMatrix y2 = SubRow(_.Y,VCol2Set(Y2));
    VMatrix Y.dist = RPow(((y1-y2)^2)*Constant(_.n,1,1),1/2);
    VMatrix R1 = get.radius(Y1);
    VMatrix R2 = get.radius(Y2);
    VMatrix r = (R1+R2-Y.dist)/2;
    VMatrix c1 = (R1-r)$/Y.dist;
    VMatrix c2 = (R2-r)$/Y.dist;
    VMatrix C = Eye(_.Tz,_.Tz, 0, c1)*y1 + 
                Eye(_.Tz,_.Tz, 0, c2)*y2;
  //VMatrix v = Gaussian(_.Tz,_.n,0,1);
  //VMatrix u = v * Eye(_.Tz, _.Tz, 0, ((v^2)*Constant(_.n,1,1))^-1/2);
  //VMatrix _.Z := C + u $* r $* Rand(_.Tz,0,1);
    VMatrix _.Z := C;
    VMatrix Z.Y.neighbour = 
    {
      Matrix Z.t = DifEq(1/(1-B),Rand(_.Tz,1,1,1));
      Matrix Z.th = (Z.t << Z.t) | 
       VMat2Mat((SubCol(_.Z.h,[[1]])<<SubCol(_.Z.h,[[2]])));
      Matrix Z.Y.idx = PivotByRows(Z.th,Sort(Z.th,[[2]]));
      Mat2VMat(Z.Y.idx)
    };
    VMatrix aux = MatQuery::SortAndCount(SubCol(Z.Y.neighbour,[[2]]));
    VMatrix _.Yt := Triplet(VMat2Mat(SubCol(aux,[[1]])) |  
                            Rand(VRows(aux),1,1,1) | 
                            VMat2Mat(SubCol(aux,[[2]])),_.H,1);
/*
    VMatrix _.Yt := 
    {
      VMatrix aux= Group("ConcatColumns",match_);
      aux*Constant(_.X.maxNeighbour-1,1,1)
    };
*/
    Real _.Mz := Case(
      prior.Z.order< 0, 0,
      prior.Z.order==0, _.Tz,
      prior.Z.order==1, _.Tz*(1+_.n),
      prior.Z.order==2, _.Tz*(1+_.n*3/2+(1/2)*_.n^2));
    _.Tz
  })}


};
