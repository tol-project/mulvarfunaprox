/* -*- mode: C++ -*- */
//////////////////////////////////////////////////////////////////////////////
// FILE   : DesignC2.tol
// PURPOSE: Defines Class @DesignC2
//////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////
Text _.autodoc.member.DesignC2 = 
"";
Class @DesignC2
//////////////////////////////////////////////////////////////////////////////
{
//Nodal points
  VMatrix _.X;
  Real _.n;
  Real _.J;

//////////////////////////////////////////////////////////////////////////////
//Configuration parameters
//////////////////////////////////////////////////////////////////////////////
  Real prior.Z.order = 2;
  Real prior.B.order = 1;
  Real min.contrast.surface = 3;

//////////////////////////////////////////////////////////////////////////////
//Auxiliar members
//////////////////////////////////////////////////////////////////////////////

  Real _.X.maxNeighbour = ?;
  Real _.Z.maxNeighbour = ?;
  VMatrix _.Y = Rand(0,2,0,0);
  VMatrix _.Yj = Rand(0,1,0,0);
  VMatrix _.Yt = Rand(0,1,0,0);
  VMatrix _.Yb = Rand(0,1,0,0);
  VMatrix _.Ym = Rand(0,1,0,0);
  VMatrix _.Yn = Rand(0,1,0,0);
  VMatrix _.Z = Rand(0,2,0,0);
  VMatrix _.Z.h = Rand(0,1,0,0);
  VMatrix _.B = Rand(0,2,0,0);
  VMatrix _.B.h = Rand(0,0,0,0);
  Real _.K = 0;
  Real _.H = 0;
  Real _.Tz = 0;
  Real _.Mz = 0;
  Real _.Tb = 0;
  Real _.Mb = 0;
  Real _.M = 0;
  Real _.N = 0;
  Set _.base = Copy(Empty);
  Real _.base.sel = ?;
  VMatrix _.QX = Rand(0,0,0,0);
  VMatrix _.QB = Rand(0,0,0,0);
  VMatrix _.PZ = Rand(0,0,0,0);
  VMatrix _.Q = Rand(0,0,0,0);
  VMatrix _.QtQ = Rand(0,0,0,0);
  VMatrix _.LQ = Rand(0,0,0,0);
  VMatrix _.Q.nonNullCellsByColumns = Rand(0,0,0,0);

  //////////////////////////////////////////////////////////////////////////////
  NameBlock get.base(Real sel)
  //////////////////////////////////////////////////////////////////////////////
  {
    Real sel_ = If(Or(IsUnknown(sel),sel<=0,sel>Card(_.base)),_.base.sel);
    _.base[sel_]
  };

  //////////////////////////////////////////////////////////////////////////////
  //Virtual methods
  //////////////////////////////////////////////////////////////////////////////
  Set get.X.weight(Set X.dif, Set X.h);
  Set get.X.weight.partial.derivative.1(Set X.dif, Set X.h, Real s);
  Real build.base(Real void);
  Real build.reference.points(Real void);
  Real build.stress.points(Real void);
  Real build.border.points(Real void);
  Real build.model.design(Real void);
  VMatrix match.evaluable(VMatrix X);
  VMatrix closest.reference.index(VMatrix X);
  VMatrix generate.internal.points(Real m);

  //////////////////////////////////////////////////////////////////////////////
    VMatrix build.obs(VMatrix X)
  //////////////////////////////////////////////////////////////////////////////
  {
    Text _MID = "[@DesignC2::build.obs] ";
    FunRn2R::@BaseC2.Eval.Function eval = 
     FunRn2R::@BaseC2.Eval.Function::New(get.base(0));
    Real m = VRows(X);
    VMatrix X.h=closest.reference.index(X);
    WriteLn("TRACE "+_MID+"1 m="<<m);
    If(!m, Rand(0,_.N,0,0), {
    WriteLn("TRACE "+_MID+"2");
    VMatrix X.j=DifEq(1/(1-B),Rand(m,1,1,1));
    WriteLn("TRACE "+_MID+"3 X.h:\n"<<X.h);
    Real numNeighbour = VColumns(X.h);
    Set X.h_ = For(1,numNeighbour,VMatrix(Real i) { SubCol(X.h,[[i]]) });
    WriteLn("TRACE "+_MID+"3.1");
    Set X.dif = For(1,numNeighbour,VMatrix(Real i)
    {
      Set X.h.i = VCol2Set(X.h_[i]);
      X - SubRow(_.Y,X.h.i)
    });
    WriteLn("TRACE "+_MID+"4");
    Set X.weight = get.X.weight(X.dif, X.h_);
    WriteLn("TRACE "+_MID+"5");
    VMatrix YnCum = DifEq(1/(1-B),_.Yn);
    VMatrix YnCumB = Sub(SetCol([[0]])<<YnCum,1,1,_.H,1);
    Matrix QX.triplet = Group("ConcatRows", For(1,numNeighbour,Matrix(Real i)
    {
      VMatrix w.i = X.weight[i];
      Set sel.rows = MatQuery::SelectMatch(NE(Drop(Abs(w.i),1.E-15),w.i*0));
      VMatrix wi = SubRow(w.i,sel.rows);
      VMatrix X.hi = SubCol(X.h,[[i]]);
      Set X.hi.set = VCol2Set(X.hi);
      VMatrix X.hiK0 = SubRow(SubRow(YnCumB,X.hi.set),sel.rows);
      VMatrix X.hiK1 = SubRow(SubRow(YnCum, X.hi.set),sel.rows);
      VMatrix Xdif.i = SubRow(X.dif[i],sel.rows);
      VMatrix X.j.i = SubRow(X.j,sel.rows);
      Real eval::upgrade.cache(Xdif.i);
      Group("ConcatRows", For(1,_.K,Matrix(Real k)
      {
        VMatrix pk = eval::evaluate.v(k,Xdif.i);
        VMatrix qk = wi$*pk; 
        VMatrix X.hik = X.hiK0+k;
        VMatrix match = LE(X.hik,X.hiK1);
        SubRow(VMat2Mat(X.j.i | (X.hik) | qk),MatQuery::SelectMatch(match))
      }))
    }));
    WriteLn("TRACE "+_MID+"6 Rows(QX.triplet)="<<Rows(QX.triplet));
    Pack(Convert(Triplet(QX.triplet, m, _.N),"Cholmod.R.Sparse"))
  })};


  //////////////////////////////////////////////////////////////////////////////
    VMatrix build.obs.partial.derivative.1(VMatrix X, Real s)
  //////////////////////////////////////////////////////////////////////////////
  {
    FunRn2R::@BaseC2.Eval.Function eval = 
     FunRn2R::@BaseC2.Eval.Function::New(get.base(0));
    FunRn2R::@BaseC2.Eval.Partial.Derivative.1 der = 
     FunRn2R::@BaseC2.Eval.Partial.Derivative.1::New(get.base(0), s);
    Text _MID = "[@DesignC2::build.obs.partial.derivative.1] ";
    Real m = VRows(X);
    VMatrix X.h=closest.reference.index(_.X);
    WriteLn("TRACE "+_MID+"1 m="<<m);
    If(!m, Rand(0,_.N,0,0), {
    WriteLn("TRACE "+_MID+"2");
    WriteLn("TRACE "+_MID+"3");
    VMatrix X.j=DifEq(1/(1-B),Rand(m,1,1,1));
    WriteLn("TRACE "+_MID+"4");
    Real numNeighbour = VColumns(X.h);
    Set X.h_ = For(1,numNeighbour,VMatrix(Real i) { SubCol(X.h,[[i]]) });
    Set X.dif = For(1,numNeighbour,VMatrix(Real i)
    {
      Set X.h.i = VCol2Set(X.h_[i]);
      X - SubRow(_.Y,X.h.i)
    });
    WriteLn("TRACE "+_MID+"5");
    Set X.weight = get.X.weight(X.dif, X.h_);
    Set X.weight.der = get.X.weight.partial.derivative.1(X.dif,X.h_,s);
    WriteLn("TRACE "+_MID+"6");
    VMatrix YnCum = DifEq(1/(1-B),_.Yn);
    VMatrix YnCumB = Sub(SetCol([[0]])<<YnCum,1,1,_.H,1);
    Matrix QX.triplet = Group("ConcatRows", For(1,numNeighbour,Matrix(Real i)
    {
      VMatrix w.i = X.weight[i];
      Set sel.rows = MatQuery::SelectMatch(NE(Drop(Abs(w.i),1.E-15),w.i*0));
      VMatrix wi = SubRow(w.i,sel.rows);
      VMatrix wi.der = SubRow(X.weight.der[i],sel.rows);
      VMatrix X.hi = SubCol(X.h,[[i]]);
      Set X.hi.set = VCol2Set(X.hi);
      VMatrix X.hiK0 = SubRow(SubRow(YnCumB,X.hi.set),sel.rows);
      VMatrix X.hiK1 = SubRow(SubRow(YnCum, X.hi.set),sel.rows);
      VMatrix Xdif.i = SubRow(X.dif[i],sel.rows);
      VMatrix X.j.i = SubRow(X.j,sel.rows);
      Real eval::upgrade.cache(Xdif.i);
      Real der::upgrade.cache(Xdif.i);
      Group("ConcatRows", For(1,_.K,Matrix(Real k)
      {
        VMatrix pk = eval::evaluate.v(k,Xdif.i);
        VMatrix pk.der = der::evaluate.v(k,Xdif.i);
        VMatrix qk = wi$*pk.der+wi.der$*pk; 
        VMatrix X.hik = X.hiK0+k;
        VMatrix match = LE(X.hik,X.hiK1);
        SubRow(VMat2Mat(X.j.i | (X.hik) | qk),MatQuery::SelectMatch(match))
      }))
    }));
    WriteLn("TRACE "+_MID+"7");
    Pack(Convert(Triplet(QX.triplet, m, _.N),"Cholmod.R.Sparse"))
  })};

  //////////////////////////////////////////////////////////////////////////////
    VMatrix build.border.prior(Real void)
  //////////////////////////////////////////////////////////////////////////////
  {
    If(!_.Tb, Rand(0,_.N,0,0), {
    VMatrix B.t=DifEq(1/(1-B),Rand(_.Tb,1,1,1));
    VMatrix Y = SubRow(_.Y, VCol2Set(_.B.h));
    VMatrix B.dif = _.B - Y;
    FunRn2R::@BaseC2 b = get.base(0);    
    VMatrix YnCum = DifEq(1/(1-B),_.Yn);
    VMatrix YnCumB = Sub(SetCol([[0]])<<YnCum,1,1,_.H,1);
    Matrix PB.triplet = 
    {
    //VMatrix B.hK = (_.B.h-1)*_.K;
      Set B.h.set = VCol2Set(_.B.h);
      VMatrix B.hK0 = SubRow(YnCumB,B.h.set);
      VMatrix B.hK1 = SubRow(YnCum, B.h.set);

      Real b::upgrade.cache(B.dif,prior.B.order,0);
      Real lastRow = 0;
      Group("ConcatRows", For(1,_.K,Matrix(Real k)
      {
        Real cumRows = lastRow;
        VMatrix pk = b::function.v(k,B.dif);
        VMatrix B.hk = B.hK0+k;
        VMatrix match = LE(B.hk,B.hK1);
        Set selRows = MatQuery::SelectMatch(match);
        {
          VMatrix row.idx = B.t+cumRows;
          Real cumRows := cumRows + _.Tb;
          SubRow(VMat2Mat(row.idx | B.hk | pk),selRows)
        }  << If(prior.B.order<1,Rand(0,3,0,0),
        Group("ConcatRows",For(1,_.n,Matrix(Real d)
        {
          VMatrix pkd = b::partial.derivative.1.v(k,B.dif,d);
          VMatrix row.idx = B.t+cumRows;
          Real cumRows := cumRows + _.Tb;
          SubRow(VMat2Mat(row.idx | B.hk | pkd),selRows)
        })))<< If(prior.B.order<2,Rand(0,3,0,0),
        Group("ConcatRows",For(1,_.n,Matrix(Real d1)
        {
          Group("ConcatRows",For(1,d1,Matrix(Real d2)
          {
            VMatrix pkd2 = b::partial.derivative.2.v(k,B.dif,d1,d2);
            VMatrix row.idx = B.t+cumRows;
            Real cumRows := cumRows + _.Tb;
            SubRow(VMat2Mat(row.idx | B.hk | pkd2),selRows)
          }))
        })))
      }))
    };
    Convert(Triplet(PB.triplet, _.Mb, _.N),"Cholmod.R.Sparse")
  })};
  //////////////////////////////////////////////////////////////////////////////
    VMatrix build.stress.prior(Real void)
  //////////////////////////////////////////////////////////////////////////////
  {
    If(!_.Tz, Rand(0,_.N,0,0), {
    VMatrix Z.t=DifEq(1/(1-B),Rand(_.Tz,1,1,1));
    
    Set Z.dif = For(1,_.Z.maxNeighbour,VMatrix(Real i)
    {
      _.Z - SubRow(_.Y,VCol2Set(SubCol(_.Z.h,[[i]])))
    });
    FunRn2R::@BaseC2 b = get.base(0);    
    VMatrix YnCum = DifEq(1/(1-B),_.Yn);
    VMatrix YnCumB = Sub(SetCol([[0]])<<YnCum,1,1,_.H,1);
    Matrix PZ.triplet = Group("ConcatRows", For(1,_.Z.maxNeighbour,Matrix(Real i)
    {
      VMatrix Z.hi = SubCol(_.Z.h,[[i]]);
      Set Z.hi.set = VCol2Set(Z.hi);
      VMatrix Z.hiK0 = SubRow(YnCumB,Z.hi.set);
      VMatrix Z.hiK1 = SubRow(YnCum, Z.hi.set);

      VMatrix Zdif.i = Z.dif[i];
      VMatrix Z.t.i = Z.t;
      Real b::upgrade.cache(Zdif.i,prior.Z.order,0);
      Real lastRow = 0;
      Group("ConcatRows", For(1,_.K,Matrix(Real k)
      {
        Real cumRows = lastRow;
        VMatrix pk = b::function.v(k,Zdif.i)*If(i==1,+1,-1);
        VMatrix Z.hik = Z.hiK0+k;
        VMatrix match = LE(Z.hik,Z.hiK1);
        Set selRows = MatQuery::SelectMatch(match);
        {
          VMatrix row.idx = Z.t.i+cumRows;
          Real cumRows := cumRows + _.Tz;
          SubRow(VMat2Mat(row.idx | Z.hik | pk),selRows)
        }  << If(prior.Z.order<1,Rand(0,3,0,0),
        Group("ConcatRows",For(1,_.n,Matrix(Real d)
        {
          Real sign = If(i==1,+1/2,-1/2);
          VMatrix pkd = b::partial.derivative.1.v(k,Zdif.i,d)*sign;
          VMatrix row.idx = Z.t.i+cumRows;
          Real cumRows := cumRows + _.Tz;
          SubRow(VMat2Mat(row.idx | Z.hik | pkd),selRows)
        })))<< If(prior.Z.order<2,Rand(0,3,0,0),
        Group("ConcatRows",For(1,_.n,Matrix(Real d1)
        {
          Group("ConcatRows",For(1,d1,Matrix(Real d2)
          {
            Real sign = If(i==1,+1/4,-1/4);
            VMatrix pkd2 = b::partial.derivative.2.v(k,Zdif.i,d1,d2)*sign;
            VMatrix row.idx = Z.t.i+cumRows;
            Real cumRows := cumRows + _.Tz;
            SubRow(VMat2Mat(row.idx | Z.hik | pkd2),selRows)
          }))
        })))
      }))
    }));
    Convert(Triplet(PZ.triplet, _.Mz, _.N),"Cholmod.R.Sparse")
  })};

  //////////////////////////////////////////////////////////////////////////////
    Real _build.model.design(Real void)
  //////////////////////////////////////////////////////////////////////////////
  {
    Text _MID = "[@DesignC2::_build.model.design] ";
    WriteLn("TRACE "+_MID+"1");
    Real build.reference.points(0);
    WriteLn("TRACE "+_MID+"2");
    Real build.stress.points(0);
    WriteLn("TRACE "+_MID+"3");
    Real build.border.points(0);
    WriteLn("TRACE "+_MID+"4");
    VMatrix _.Ym := 
      _.Yj+
      _.Yt*Case(
      prior.Z.order< 0, 0,
      prior.Z.order==0, 1,
      prior.Z.order==1, 1*(1+_.n),
      prior.Z.order==2, 1*(1+_.n*3/2+(1/2)*_.n^2))+
      _.Yb*Case(
      prior.B.order< 0, 0,
      prior.B.order==0, 1,
      prior.B.order==1, 1*(1+_.n),
      prior.B.order==2, 1*(1+_.n*3/2+(1/2)*_.n^2));
    WriteLn("TRACE "+_MID+"5");
    Real _.M := _.J+_.Mb+_.Mz;
    WriteLn("TRACE "+_MID+"6");
    Real build.base(0);
    WriteLn("TRACE "+_MID+"7");
    Real _.N := VMatSum(_.Yn);
    _.N<_.M
  };

  //////////////////////////////////////////////////////////////////////////////
    Real build.decomposition(Real void)
  //////////////////////////////////////////////////////////////////////////////
  {
    Text _MID = "[@DesignC2::build.decomposition] ";
    WriteLn("TRACE "+_MID+"1");
    VMatrix _.QX := build.obs(_.X);
    WriteLn("TRACE "+_MID+"2");
    VMatrix _.QB := build.border.prior(0);
    WriteLn("TRACE "+_MID+"3");
    VMatrix _.PZ := build.stress.prior(0);
    WriteLn("TRACE "+_MID+"4");
    VMatrix _.Q := _.QX << _.QB << _.PZ;
    WriteLn("TRACE "+_MID+"5");
/*
    VMatrix _.Q.nonNullCellsByColumns := 
    {
      VMatrix aux = Tra(Rand(1,_.M,1,1)*Or(_.Q,Zeros(_.M,_.N)));
      Group("ConcatColumns",For(1,_.K,VMatrix(Real k)
      {
        SubRow(aux,Range(k,_.N,_.K)) 
      }))
    };
*/
    WriteLn("TRACE "+_MID+"6");
    VMatrix _.QtQ := MtMSqr(_.Q);
    VMatrix _.LQ := CholeskiFactor(_.Q,"XtX");
/*
    VMatrix _.LQ := CholeskiFactor(_.QtQ,"X",False,True);
    Real iter = 1;
    Real lambda = 1.e-15;
    While(And(!VRows(_.LQ), iter<=10),
    {
      VMatrix _.QtQ := _.QtQ + Eye(_.N,_.N,0,lambda);
      VMatrix _.LQ := CholeskiFactor(_.QtQ,"X",False,True);
      Real lambda := lambda * 2;
      Real iter := iter+1
    });
*/
    WriteLn("TRACE "+_MID+"7");
    True
  }
};

